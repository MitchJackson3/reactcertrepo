import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Jokes from "../components/Jokes";
import { useStore } from "../Store";
jest.mock("../Store");


let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('testing the app component', () => {
  let today = new Date().toString();
  useStore.mockReturnValue({
    store: {
      viewedJokes: [
        {
          key: 1,
          category: 'animal',
          joke: 'Chuck Norris is my dad.',
          dateViewed: today
        },
        {
          key: 2,
          category: 'career',
          joke: 'Chuck Norris is the boss.',
          dateViewed: today
        },
      ]
    }
  })

  act(() => {
    render(<Jokes />, container);
  });

  expect(container.textContent).toContain("Chuck Norris is my dad.");
  expect(container.textContent).toContain("Chuck Norris is the boss.");
})