import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Search from "../components/Search";
import { useStore } from "../Store";
jest.mock("../Store");

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
})

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('testing the search component', async () => {
  useStore.mockReturnValue({
    store: {
      matchingJokes: [{
        categories: ["animal"],
        value: "chuck norris animal",
      }, {
        categories: ["career"],
        value: "chuch norris career"
      }],
      fetchMatchingJokes: () => {}
    }
  })

  act(() => {
    render(<Search />, container);
  });

  const searchInput = screen.getByRole('searchInput');
  const searchButton = screen.getByRole('searchButton');

  fireEvent.change(searchInput, { target: { value: 'animal' } });
  fireEvent.click(searchButton);

  await new Promise((resolve, reject) => {
    setTimeout(resolve, 5000);
  });

  act(() => {
    render(<Search />, container);
  });

  expect(screen.getByRole('jokeList').textContent).toContain("animal");
});