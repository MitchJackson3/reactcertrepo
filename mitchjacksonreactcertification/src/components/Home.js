import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Spinner from 'react-easy-spinner';
import { storeContext } from "../Store";
import { observer } from "mobx-react";
import { ProtectedNavItem } from "../App";
import styles from "../styles.module.css";
import Categories from "./Categories.js";
import Search from "./Search.js";
import Jokes from "./Jokes.js";

@observer
class HomePage extends Component{
	constructor(props) {
		super(props);

		this.state = {
			hidden: true,
			email: "abc@123.com",
			password: "abcdWE1",
			loginDisabled: true,
			showSpinner: false
		};
	}

	store = {};

	UNSAFE_componentWillMount() {
		this.store = this.context.store;
  }

	updateLoginButtonState() {
		var pwRegEx = /(?=.*[A-Z])(?=.*[a-z])(?=.*\d)/;
		if (document.getElementById("email").value != "" &&
			document.getElementById("email").value.indexOf("@") >= 0 &&
			document.getElementById("email").value.indexOf(".com") >= 0 &&
			document.getElementById("password").value != "" &&
			document.getElementById("password").value.length > 5 &&
			document.getElementById("password").value.length < 11 &&
			pwRegEx.test(document.getElementById("password").value)){
			this.setState({ loginDisabled: false });
		}
		else {
			this.setState({ loginDisabled: true });
		}
	}

	handleEmailChange = (e) => {
		this.setState({ email: e.target.value });

		this.updateLoginButtonState();
	}

	handlPWChange = (e) => {
		this.setState({ password: e.target.value });

		this.updateLoginButtonState();
	}

	toggleShow() {
		this.setState({ hidden: !this.state.hidden });
	}

	logIn = () => {
		this.showHideSpinner()

		let userInfo = {
			email: this.state.email,
			password: this.state.password
        }

		let userJsonString = JSON.stringify(userInfo);
		var userJson = JSON.parse(userJsonString);

		var millisecondsToWait = 1000;
		setTimeout(this.showHideSpinner, millisecondsToWait);
		this.store.enableNav();
	}

	showHideSpinner = () => {
		this.setState({ showSpinner: !this.state.showSpinner });
    }

	render() {
		let settings = {
			shape: "triangleUp",
			animation: "pulse",
			time: "2s",
			duration: 'infinite',
			opacity: '0.3',
			bgColor: '#27556c',
			elColor: '#2d1557'
		}

		return (
			<div>
				<h2>Home</h2>
				{!this.store.loggedIn ?
					(<>
						<div>
						<label>Email</label>
						<input id="email" name='email' value={this.state.email} onLoad={this.getInitialState} onChange={this.handleEmailChange} />
						</div>
						<div>
							<label>Password</label>
							<input role="passwordInput" id="password" type={this.state.hidden ? "password" : "text"} value={this.state.password} name='password' onChange={this.handlPWChange} />
							<button onClick={() => this.toggleShow()}>Show/Hide Password</button>
						</div>
						<div>
							<button disabled={this.state.loginDisabled} onClick={this.logIn}>Log In</button>
						</div>
						<div id='pwCapitalLetterReq' className="errorText">A valid email must be entered!</div>
						<div id='pwCapitalLetterReq' className="errorText">Your password must contain a capital letter!</div>
						<div id='pwLowerCaseReq' className="errorText">Your password must contain a lower case letter!</div>
						<div id='pwNumberReq' className="errorText">Your password must contain a number!</div>
						<div id='pwLengthReq' className="errorText">Your password must be 6 - 10 characters in length!</div>
					</>) :
					('')}
				{this.state.showSpinner ? <Spinner id='spinner'  {...settings} /> : null}
				{this.store.loggedIn ?
					<Router>
							<h3 className={styles.testClass}><a href='/categories' loggedIn={this.store.loggedIn}>Categories</a>: The categories of jokes about Chuck Norris.</h3>
							<h3><a href='/search' loggedIn={this.store.loggedIn}>Search</a>: This is where you can search for jokes about Chuck Norris.</h3>
							<h3><a href='/jokes' loggedIn={this.store.loggedIn}>Jokes</a>: This is where the jokes about Chuck Norris are listed.</h3>
					</Router> :
				('')}
			</div>
		);
	}
}

HomePage.contextType = storeContext;

export default HomePage;