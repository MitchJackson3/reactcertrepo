export const MODAL_TRIGGER = 'MODAL_TRIGGER';
export const JOKES_LIST = 'JOKES_LIST';
export const FETCH_CATEGORIES = 'MODAL_TRIGGER';

export const showJokeWindow = () => ({
    type: MODAL_TRIGGER,
    payload: false
});

export const fetchJoke = (searchTerm) => ({
    type: JOKES_LIST,
    payload: searchTerm
});

export const fetchCategories = () => ({
    type: FETCH_CATEGORIES,
    payload: []
});