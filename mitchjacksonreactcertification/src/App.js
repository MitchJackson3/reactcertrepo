import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./components/Home.js";
import Categories from "./components/Categories.js";
import Search from "./components/Search.js";
import Jokes from "./components/Jokes.js";
import { storeContext } from "./Store";
import { observer } from "mobx-react";

@observer
class App extends Component {
  store = {};

  UNSAFE_componentWillMount() {
    // delete this
  //  this.enableNav();

    this.store = this.context.store;
  }

  render() {
    return (
      <div id="root2">
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <ProtectedNavItem
                  text="Categories"
                  path="/categories"
                  loggedIn={this.store.loggedIn}
                />
                <ProtectedNavItem
                  text="Search"
                  path="/search"
                  loggedIn={this.store.loggedIn}
                />
                <ProtectedNavItem
                  text="Jokes"
                  path="/jokes"
                  loggedIn={this.store.loggedIn}
                />
              </ul>
            </nav>

            {/* A <Switch> looks through its children <Route>s and
                         renders the first one that matches the current URL. */}
            <Switch>
              <Route path="/jokes">
                <Jokes />
              </Route>
              <Route path="/categories">
                <Categories />
              </Route>
              <Route path="/search">
                <Search />
              </Route>
              <Route path="/">
                <Home
                  //enableNav={() => this.store.enableNav}
                  //loggedIn={this.store.loggedIn}
                />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

App.contextType = storeContext;

export default App;

export const ProtectedNavItem = props => {
  return (
    <li>
      {!props.loggedIn ? (
        <Link
          to={props.path}
          className="disabledCursor"
          onClick={event => event.preventDefault()}
        >
          {props.text}
        </Link>
      ) : (
        <Link to={props.path} className="notDisabled">
          {props.text}
        </Link>
      )}
    </li>
  );
};
