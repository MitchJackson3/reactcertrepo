module.exports = {
  presets: [[
    '@babel/preset-env', {'useBuiltIns': 'entry'}],
    '@babel/preset-react',
    '@babel/preset-typescript',
  ],
  plugins: [
    ['@babel/plugin-proposal-decorators', { 'legacy': true }],
    ['@babel/plugin-proposal-class-properties', { 'loose': true }],
    '@babel/plugin-proposal-export-default-from',
    ['@babel/plugin-transform-runtime', {'regenerator': true}],
  ], 
};