import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import App from "../App";


let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('testing the app component', () => {
  act(() => {
    render(<App />, container);
  });
  expect(screen.getAllByText('Home')[0].classList.contains("disabledCursor")).toBe(false);
  expect(screen.getByText('Categories').classList.contains("disabledCursor")).toBe(true);
  expect(screen.getByText('Search').classList.contains("disabledCursor")).toBe(true);
  expect(screen.getByText('Jokes').classList.contains("disabledCursor")).toBe(true);
})