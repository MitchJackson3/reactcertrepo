import { observable, action, flow } from "mobx";
import React from "react";


class Store {

  key = 0;

  @observable categories = [];

  @observable isLoaded = false;

  @observable error = "";

  @observable currentJoke = { value: "No match." };

  @observable matchingJokes = [];

  @observable showJokeModal = false;

  @observable viewedJokes = [];

  @observable loggedIn = false;

  @action
  enableNav = () => {
    this.loggedIn = !this.loggedIn;
  };

  @action
  async fetchCategories () {
    this.error = "";
    this.isLoaded = false;

    try {
      const res = await window.fetch("https://api.chucknorris.io/jokes/categories");
      const data = await res.json();
      this.categories = data;
    } catch (e) {
      this.error = e.message;
    } finally {
      this.isLoaded = true;
    }
  }

  @action
  async fetchRandomJoke(searchTerm) {
    this.currentJoke = { value: "No match." };

    const res = await window.fetch(
      "https://api.chucknorris.io/jokes/search?query=" + searchTerm
    );
    const result = await res.json();
    let rand = Math.floor(Math.random() * result.result.length);

    if (result.result.length == 0) {
      this.showJokeModal = true;
    }
    else {
      this.currentJoke = result.result[rand]; 

      this.showJokeModal = true;
      let today = new Date().toString();

      let joke = this.findCategory(result.result[rand]);
      this.viewedJokes.push({
        key: this.key++,
        category: joke.categories[0],
        joke: joke.value,
        dateViewed: today
      });
    }
  }

  @action
  async fetchMatchingJokes(searchTerm) {
    const res = await window.fetch("https://api.chucknorris.io/jokes/search?query=" + searchTerm);
    const result = await res.json();

    this.matchingJokes = result ? (result.result || []) : [];
  }

  @action
  fetchJoke(joke) {
    this.currentJoke = joke.value;
    this.showJokeModal = true;
    let today = new Date().toString();

    joke = this.findCategory(joke);
    this.viewedJokes.push({
      key: this.key++,
      category: joke.categories[0],
      joke: joke.value,
      dateViewed: today
    });
  }

  @action
  closeModal() {
    this.showJokeModal = false;
  }

  findCategory(joke) {
    this.categories.forEach(
      category => {
        if (joke.value.indexOf(category) > -1) {
          joke.categories.push(category);
        }
      });

    return joke;
  }
}

export default Store;

export const storeContext = React.createContext({
  store: new Store()
});

export const useStore = () => React.useContext(storeContext);
