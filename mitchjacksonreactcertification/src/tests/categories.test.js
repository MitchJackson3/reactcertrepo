import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Categories from "../components/Categories";
import { useStore } from "../Store";
jest.mock("../Store");


let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('testing the app component', async () => {
  useStore.mockReturnValue({
    store: {
      isLoaded: true,
      error: "",
      categories: [
        "animal",
        "career"
      ],
      showJokeModal: false,
      currentJoke: {
        value: "This is the current joke."
      },
      fetchRandomJoke: () => {
        
      },
    }
  })

  act(() => {
    render(<Categories />, container);
  });

  fireEvent.click(screen.getByText('animal'));
  
  expect(container.textContent).toContain("animal");
})