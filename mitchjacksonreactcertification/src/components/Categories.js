import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import Modal, { closeStyle } from "simple-react-modal";
import { observer } from "mobx-react";
import { useStore } from "../Store";

const Categories = observer(() => {
  const { store } = useStore();

  useEffect(() => {
    if ((store.categories || []).length == 0) {
      store.fetchCategories();
    }
  })

  return (
    <>
      {store.error ? (
        <div>Error: {store.error}</div>
      ) : !store.isLoaded ? (
        <div>Loading...</div>
      ) : (
            <>
              <h2>Categories:</h2>
              <ul>
                {store.categories.map(item => (
                  <li key={item}>
                    <a onClick={() => store.fetchRandomJoke(item)}>{item}</a>
                  </li>
                ))}
              </ul>
              <Modal
                show={store.showJokeModal}
                onClose={() => store.closeModal()}
              >
                {store.currentJoke.value}
                <a style={closeStyle} onClick={() => store.closeModal()}>
                  X
            </a>
              </Modal>
            </>
          )}
    </>
  );
});

export default Categories;
