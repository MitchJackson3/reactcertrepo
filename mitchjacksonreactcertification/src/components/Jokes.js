import React, { Component } from "react";
import { observer } from "mobx-react";
import { useStore } from "../Store";
import { MDBDataTableV5 } from "mdbreact";

const Jokes = observer(() => {
  const { store } = useStore();
  const data = {
    columns: [
      {
        label: "Category",
        field: "category",
        width: 150
      },
      {
        label: "Joke",
        field: "joke",
        width: 150
      },
      {
        label: "Viewed",
        field: "dateViewed",
        width: 150
      }
    ],
    rows: store.viewedJokes
  };

  return (
    <>
      <h2>Jokes</h2>
      <MDBDataTableV5 striped bordered small data={data} />
    </>
  );
});

export default Jokes;
