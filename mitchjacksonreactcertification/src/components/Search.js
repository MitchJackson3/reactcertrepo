import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Modal, { closeStyle } from "simple-react-modal";
import { observer } from "mobx-react";
import { useStore } from "../Store";
import { BrowserRouter as Router } from "react-router-dom";

const Search = observer(() => {
	const [searchText, setSearchText] = useState('');
	const { store } = useStore();
	useEffect(() => {
		console.log(store.matchingJokes.length);
		return function cleanup() {
			console.log("bye felicia");
    }
	}, [store.matchingJokes]);

	const handleSearchChange = (e) => {
		setSearchText(e.target.value);
	}

	return (<>
		<div>
			<h2>Search</h2>
			<input role="searchInput" value={searchText} onChange={handleSearchChange}></input>
			<button role="searchButton" onClick={() => store.fetchMatchingJokes(searchText)}>Search</button>
		</div>
		<Router>
			<ul role="jokeList">
				{store.matchingJokes.map(item => (
					<li key={item.value}>
						<Link onClick={() => store.fetchJoke(item)}>
							{item.value.length > 50 ?
								(
									<label>{item.value.substring(0, 50)}</label>
								) :
								(
									<label>{item.value}</label>
								)}
						</Link>
					</li>
				))}
			</ul>
		</Router>
		<Modal
			show={store.showJokeModal}
			onClose={() => store.closeModal()}
		>
			{store.currentJoke}
			<a style={closeStyle} onClick={() => store.closeModal()}>
				X
      </a>
		</Modal>
	</>
	);
});

export default Search;