import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import Home from "../components/Home";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
})

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('testing the home component', async () => {
  act(() => {
    render(<Home />, container);
  });
  expect(container.textContent).toContain("Home");
  expect(container.textContent).toContain("Email");
  expect(container.textContent).toContain("Password");

  const passwordInput = screen.getByRole('passwordInput');
  const loginButton = screen.getByText('Log In');

  fireEvent.change(passwordInput, { target: { value: 'abcdWE1a' } });

  expect(loginButton.disabled).toBe(false);

  try {
    fireEvent.click(loginButton);
  }
  catch (e) {}

  await new Promise((resolve, reject) => {
    setTimeout(resolve, 1500);
  })

  act(() => {
    render(<Home />, container);
  });

  expect(screen.getByText('Categories')).toBeTruthy();
});