import { MODAL_TRIGGER, JOKES_LIST, FETCH_CATEGORIES } from './Actions';

export const JokeModalToggle = (state = false, action) => {
    switch (action.type) {
        case MODAL_TRIGGER:
            return action.payload;
        default:
            return state;
    }
}

export const CurrentJoke = (state = '', action) => {
    switch (action.type) {
        case JOKES_LIST:
            const res = fetch(
                "https://api.chucknorris.io/jokes/search?query=" + action.payload
            );
            const result = res.json();
            let rand = Math.floor(Math.random() * result.result.length);
            return result.result[rand].value;
        default:
            return state;
    }
}

export const CategoriesList = (state = [], action) => {
    switch (action.type) {
        case FETCH_CATEGORIES:
            fetch("https://api.chucknorris.io/jokes/categories")
                .then(res => res.json())
                .then(
                    result => {
                        return result;
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    error => {
                        throw error;
                    }
                );
        default:
        return state;
    }
}